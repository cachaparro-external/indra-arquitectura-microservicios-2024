FROM eclipse-temurin:11

RUN apt update
RUN mkdir /opt/app
RUN mkdir /opt/app/storage

VOLUME /opt/app/storage

#ARG MAX_MEM=256
#ENV MAX_MEM=256

COPY target/test-rest-api-indra-2024-1.0.0.jar /opt/app/app.jar

EXPOSE 8989

#CMD ["java", "-jar", "-Xms128m -Xmx256m",  "/opt/app/app.jar"]
CMD ["java", "-jar",  "/opt/aspp/app.jar"]