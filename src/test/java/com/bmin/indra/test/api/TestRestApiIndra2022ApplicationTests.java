package com.bmin.indra.test.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import com.bmind.indra.test.api.TestRestApiIndra2022Application;

@SpringBootTest(webEnvironment = WebEnvironment.NONE, classes = {TestRestApiIndra2022Application.class})
class TestRestApiIndra2022ApplicationTests {

	@Value("${spring.application.name}")
	String applicationName;
	
	/*@Autowired
	private EurekaClient client;
	
	@Test
	void contextLoads() {
		List<InstanceInfo> instances = this.client.getApplication(applicationName).getInstances();
		
		instances.forEach(instance -> {
			System.out.println(instance.getHostName() + " - " + instance.getPort());
		});
	
	}*/

}
